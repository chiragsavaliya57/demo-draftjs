import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

import { userActions } from '../_actions';

import * as projectService from '../_services/project.service';
import localStorage from 'local-storage';

class HomePage extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
          projectList:[],
          setSpeakerList:[],
        };
        this.setSpeakerListFunction  = this.setSpeakerListFunction.bind(this);
    }

    componentWillMount() {
      projectService.getAllProject()
        .then(response => {
            this.setState({projectList:response});
        }) 
        .catch(error => {
            console.log("error >>" + error)
        })

        this.props.getUsers();
    }
  
    setSpeakerListFunction(projectId) {

        const result = this.state.projectList.filter(project => project.id == projectId);
        console.log(result[0].medias[0].speakers);
        localStorage.set("ss.ai.spl",result[0].medias[0].speakers);
    }

    gottoEditor = ev => {
      var mediaId = ev.currentTarget.id;
      var projectId = ev.currentTarget.dataset.id;
      localStorage.set('ss.ai.mid',mediaId);
      localStorage.set("ss.ai.pid",projectId);
      this.setSpeakerListFunction(projectId);
    };

    render() {
        const { user, users } = this.props;
        return (
            <div className="home mt-5">
                <div className="row">
                {this.state.projectList.map(product =>
                    <div key={product.id} className="col-sm-3">
                    <div className={"product"} onClick={this.gottoEditor} id={product.medias[0].id} data-id={product.id}>
                        <img src={product.img} alt={product.projectName} />
                        <div className="image_overlay"/>
                        <div className="view_details">
                            <Link to={"/editor"}>Compare</Link>
                        </div>
                        <div className="stats">
                            <div className="stats-container">
                                <span className="product_name">{product.projectName}</span>
                            </div>
                        </div>
                    </div>
                    </div>          
                )}
                </div>
            </div>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAll,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };