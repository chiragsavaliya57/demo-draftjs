import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';


const highlightedTextReducer = (highLightedText = null, action) => {
  if (action.type === 'TEXT_HIGHLIGHTED') {
    return action.payload;
  }

  return highLightedText;
}

const selectedPlayTimeReducer = (selectedPlayTime = null, action) => {
  if (action.type === 'PLAY_TIME_SELECTED') {
    return action.payload;
  }

  return selectedPlayTime;
}

const changedCurrentPlayedBlockReducer = (changedCurrentPlayedBlock = null, action) => {
  if (action.type === 'CHANGED_CURRENT_PLAYED_BLOCK') {
    return action.payload;
  }

  return changedCurrentPlayedBlock;
}

const autoScrollActivatedReducer = (autoScrollActivated = { isActive: true }, action) => {
  if (action.type === 'AUTO_SCROLL_ACTIVATED') {
    return action.payload;
  }

  return autoScrollActivated;
}

const changedMediaTimeCodeReducer = (mediaTimeCode = { time: null, frames: null }, action) => {
  if (action.type === 'MEDIA_TIMECODE_CHANGED') {
    return action.payload;
  }

  return mediaTimeCode;
}

const changedPendingSavePs4Reducer = (pendingSaved = false, action) => {
  if (action.type === 'CHANGED_PENDINNG_SAVE') {
    return action.payload;
  }

  return pendingSaved;
}

const updatedSpeakerListReducer = (speakerList = [], action) => {
  if (action.type === 'ADD_SPEAKER_ITEM') {
    const existentItem = speakerList.find(speaker => speaker.id === action.payload.id)
    if (!existentItem) {
      return [...speakerList.slice(0), action.payload]
    }
  }

  if (action.type === 'UPDATE_SPEAKER_ITEM') {
    return speakerList.map(speaker => {
      if (speaker.id !== action.payload.id) {
        return speaker;
      }
      return {
        ...speaker,
        ...action.payload
      }
    })
  }

  if (action.type === 'DELETE_SPEAKER_ITEM') {
    const itemToDeleteIndex = speakerList.findIndex(speaker => speaker.id === action.payload.speakerId)
    if (itemToDeleteIndex !== -1) {
      return [...speakerList.slice(0, itemToDeleteIndex), ...speakerList.slice(itemToDeleteIndex + 1)];
    }
  }

  if (action.type === 'INIT_SPEAKER_LIST') {
    return action.payload;
  }

  return speakerList;
}

const compareApp = combineReducers({
  authentication,
  registration,
  users,
  alert,
  highlightedText: highlightedTextReducer,
  selectedPlayTime: selectedPlayTimeReducer,
  autoScrollActivated: autoScrollActivatedReducer,
  changedMediaTimeCode: changedMediaTimeCodeReducer,
  updatedSpeakerList: updatedSpeakerListReducer,
  changedPendingSavePs4: changedPendingSavePs4Reducer,
  changedCurrentPlayedBlock: changedCurrentPlayedBlockReducer
});

export default compareApp;