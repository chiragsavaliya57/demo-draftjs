import React from 'react';
import { connect } from 'react-redux';

import domHelperFactory from './dom-helper.service';

import './index.scss';

class AutoScrollButton extends React.Component {
  constructor(props) {
    super(props);

    this.domHelperService = domHelperFactory(window);

    this.state = {
      isAtTheBottom: false
    }

    this.handleScroll = () => this._handleScroll();
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.autoScrollActivated !== nextProps.autoScrollActivated ||
      this.state.isAtTheBottom !== nextState.isAtTheBottom;
  }

  _handleScroll() {
    this.setState({
      isAtTheBottom: this.domHelperService.isScrollAlmostAtBottom(60)
    });
  }

  getButtonTemplate() {
    return (
      <div className={`mousetrap resume-transcript-auto-scroll ${this.state.isAtTheBottom ? 'is-at-the-bottom': ''}`} onClick={this.props.turnOnAutoScroll}>
        <i className="fa fa-sort-amount-desc"></i>
        Resume Transcript Auto-Scroll
      </div>
    );
  }

  render() {
    return (
      <div>
        {!this.props.autoScrollActivated
          ? this.getButtonTemplate()
          : null
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { autoScrollActivated: state.autoScrollActivated.isActive };
};

export default connect(mapStateToProps, null)(AutoScrollButton);
