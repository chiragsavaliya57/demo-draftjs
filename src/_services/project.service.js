import localStorage from 'local-storage';
import axios from 'axios';

export function createRawContent(transcriptRows) {

  const blocks = [];
  const entityMap = {};

  if (transcriptRows) {
    const transcripts = transcriptRows.map(transcriptRow => {
      return {
        ...transcriptRow,
        wordsJson: JSON.parse(transcriptRow.wordsJson)
      };
    })

    transcripts.forEach((transcriptRow, rowIndex) => {
      let accumulattedOffset = 0;
      blocks.push({
        text: transcriptRow.wordsJson.map(word => word.text).join(''),
        type: 'TranscriptRow',
        data: {
          ...transcriptRow,
          speaker: transcriptRow.speaker ? JSON.stringify(transcriptRow.speaker) : ''
        },
        entityRanges: transcriptRow.wordsJson.map((word, index) => {
          const entityKey = '_' + String(rowIndex) + '_' + String(index);

          entityMap[entityKey] = {
            type: 'TIME_CODED_WORD',
            data: {
              startTime: word.startTime,
              endTime: word.endTime
            },
            mutability: 'MUTABLE'
          };

          if (index) {
            const previousText = transcriptRow.wordsJson[index - 1].text;
            accumulattedOffset += previousText.length;
          }

          return {
            offset: accumulattedOffset,
            length: word.text.length,
            key: entityKey
          };
        })
      });


    });

    return { blocks, entityMap };
  }

}


export function authHeader() {
  // return authorization header with basic auth credentials
  return {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJVUkZNalkyTVVJek9EUXhNak5GUXpJd1JESkROVEJDTVRnM056RTRSVGd3T1VNNE1VVkdPQSJ9.eyJodHRwczovL3NpbW9uc2F5cy5haS9lbWFpbCI6InNhdmFsaXlhY2hpcmFnMTgzQGdtYWlsLmNvbSIsImh0dHBzOi8vc2ltb25zYXlzLmFpL2Z1bGxOYW1lIjoiQ2hpcmFnIFNhdmFsaXlhIiwiaHR0cHM6Ly9zaW1vbnNheXMuYWkvaXNFbWFpbFZlcmlmaWVkIjp0cnVlLCJodHRwczovL3NpbW9uc2F5cy5haS9jb25uZWN0aW9uIjoiZ29vZ2xlLW9hdXRoMiIsImh0dHBzOi8vc2ltb25zYXlzLmFpL2Nvbm5lY3Rpb25TdHJhdGVneSI6Imdvb2dsZS1vYXV0aDIiLCJpc3MiOiJodHRwczovL3NpbW9uc2F5cy10ZXN0LmF1dGgwLmNvbS8iLCJzdWIiOiJnb29nbGUtb2F1dGgyfDExMTg4NzA1MzEyMjc3NzQwMDQzNiIsImF1ZCI6WyJodHRwczovL3NpbW9uc2F5c3RyYW5zY3JpYmUuY29tIiwiaHR0cHM6Ly9zaW1vbnNheXMtdGVzdC5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTcwODc1NDU3LCJleHAiOjE1NzA5NjE4NTcsImF6cCI6ImZPYXdpbDFOd29TVmkyUnUtcGpnbjBBVUZOcy03WjRQIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCByZWFkOm1lc3NhZ2VzIn0.QtF4S-nfO-OakXVvfEtRfIqnPWgiWilGH4b-bhex4t34jP4KixGmdMi_QBL9ljNEcTPQ9aSTRzat8oW5ly2uCSlR7k9qZ5uw4CLhMUBXKfP6NrBA2gcsNutszPcR_DMLgR5Rmc5B1adDrSEH80-uHVEntNv81KOBzSaIrp_kcJooHNFfgt4giO_Lu4wKdB43DkBw8w8p2SzM_jrG0UKenW-aeUSM9soZOuAEey-dSr26iHdSdb67f0adDDym5W0Jo58p8im00glkwjUigHSzKBCzf6R79L5JH0H7mdGAwtxqOWiPbfTD_InhwMuXi-g5_xcS9USl05EJ9QPnpfSiGw'
  }
}


function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    return data;
  });
}


export function getAllTranscription() {

  const mediaId = localStorage.get('ss.ai.mid');
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  };

  return fetch("http://54.218.15.119:8080/media/" + mediaId + "/transcription", requestOptions)
    .then(handleResponse)
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log('error in get user' + error);
      return error;
    });
}

export function getAllProject() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  }

  return fetch("http://54.218.15.119:8080/projects", requestOptions)
    .then(handleResponse)
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log("error in getting project");
      return error;
    })
}

export function getProject() {

  const projectId = localStorage.get('ss.ai.pid');

  const requestOptions = {
    method: 'GET',
    headers: authHeader()
  }

  return fetch("http://54.218.15.119:8080/projects/" + projectId, requestOptions)
    .then(handleResponse)
    .then(response => {
      return response;
    })
    .catch(error => {
      console.log('error in get user' + error);
      return error;
    });
}


export function saveTranscription(transcriptRow) {

  const mediaId = localStorage.get('ss.ai.mid');
  let userAccessToken = localStorage.get('ss.ai.accessToken');
  const host = localStorage.get('ss.ai.host');
  const projectId = localStorage.get('ss.ai.pid');

  const requestOptions = {
    method: 'POST',
    headers: {
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJVUkZNalkyTVVJek9EUXhNak5GUXpJd1JESkROVEJDTVRnM056RTRSVGd3T1VNNE1VVkdPQSJ9.eyJodHRwczovL3NpbW9uc2F5cy5haS9lbWFpbCI6InNhdmFsaXlhY2hpcmFnMTgzQGdtYWlsLmNvbSIsImh0dHBzOi8vc2ltb25zYXlzLmFpL2Z1bGxOYW1lIjoiQ2hpcmFnIFNhdmFsaXlhIiwiaHR0cHM6Ly9zaW1vbnNheXMuYWkvaXNFbWFpbFZlcmlmaWVkIjp0cnVlLCJodHRwczovL3NpbW9uc2F5cy5haS9jb25uZWN0aW9uIjoiZ29vZ2xlLW9hdXRoMiIsImh0dHBzOi8vc2ltb25zYXlzLmFpL2Nvbm5lY3Rpb25TdHJhdGVneSI6Imdvb2dsZS1vYXV0aDIiLCJpc3MiOiJodHRwczovL3NpbW9uc2F5cy10ZXN0LmF1dGgwLmNvbS8iLCJzdWIiOiJnb29nbGUtb2F1dGgyfDExMTg4NzA1MzEyMjc3NzQwMDQzNiIsImF1ZCI6WyJodHRwczovL3NpbW9uc2F5c3RyYW5zY3JpYmUuY29tIiwiaHR0cHM6Ly9zaW1vbnNheXMtdGVzdC5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTcwODc1NDU3LCJleHAiOjE1NzA5NjE4NTcsImF6cCI6ImZPYXdpbDFOd29TVmkyUnUtcGpnbjBBVUZOcy03WjRQIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCByZWFkOm1lc3NhZ2VzIn0.QtF4S-nfO-OakXVvfEtRfIqnPWgiWilGH4b-bhex4t34jP4KixGmdMi_QBL9ljNEcTPQ9aSTRzat8oW5ly2uCSlR7k9qZ5uw4CLhMUBXKfP6NrBA2gcsNutszPcR_DMLgR5Rmc5B1adDrSEH80-uHVEntNv81KOBzSaIrp_kcJooHNFfgt4giO_Lu4wKdB43DkBw8w8p2SzM_jrG0UKenW-aeUSM9soZOuAEey-dSr26iHdSdb67f0adDDym5W0Jo58p8im00glkwjUigHSzKBCzf6R79L5JH0H7mdGAwtxqOWiPbfTD_InhwMuXi-g5_xcS9USl05EJ9QPnpfSiGw',
      'Accept': "application/json, text/plain, */*",
      'Content-Type': "application/json;charset=utf-8",
      'X-Requested-With': "XMLHttpRequest"
    },
    body: JSON.stringify(transcriptRow),
    contentType: 'application/json',
    dataType: 'json'
  };

  var BASE_URL = 'http://54.218.15.119:8080/media/' + mediaId + '/transcript';

  return fetch(BASE_URL, requestOptions)
    .then(res => {
      console.log(res);
      return res;
    }).catch(error => {
      console.log(error);
    })

}