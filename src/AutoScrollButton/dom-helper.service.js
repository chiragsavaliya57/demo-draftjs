import React from 'react';

export default function domHelperService($window) {
  return {
    getScrollPosition: getScrollPosition,
    isScrollAtTheBottom: isScrollAtTheBottom,
    isScrollAlmostAtBottom: isScrollAlmostAtBottom,
    inIframe: inIframe
  };

  function getElementFromSelector(selector) {
    return selector ? React.findDOMNode(selector)[0] : $window;
  }

  function getScrollPosition(selector) {
    var element = getElementFromSelector(selector);
    return element.scrollY || element.scrollTop || $window.document.getElementsByTagName('html')[0].scrollTop;
  }

  function isScrollAtTheBottom(selector) {
    var element = getElementFromSelector(selector);
    return element.innerHeight + getScrollPosition(selector) >= (selector ? element.scrollHeight : $window.document.body.scrollHeight);
  }

  function isScrollAlmostAtBottom(offset, selector) {
    var element = getElementFromSelector(selector);
    return element.innerHeight + getScrollPosition(selector) + offset >= (selector ? element.scrollHeight : $window.document.body.scrollHeight);
  }

  function inIframe() {
    try {
      return $window.self !== $window.top;
    } catch (e) {
      return true;
    }
  }
}
