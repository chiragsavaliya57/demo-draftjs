export const highlightText = (currentTimeInSeconds) => {
    return {
        type: 'TEXT_HIGHLIGHTED',
        payload: {
            currentTimeInSeconds
        }
    };
};

export const selectPlayTime = (currentTimeInSeconds) => {
    return {
        type: 'PLAY_TIME_SELECTED',
        payload: {
            currentTimeInSeconds
        }
    };
};

export const activateAutoScroll = (isActive) => {
    return {
        type: 'AUTO_SCROLL_ACTIVATED',
        payload: {
            isActive
        }
    };
};

export const changeMediaTimeCode = (mediaTimeCode) => {
  return {
      type: 'MEDIA_TIMECODE_CHANGED',
      payload: {
        ...mediaTimeCode
      }
  };
};


export const addSpeakerItem = (speaker) => {
  return {
      type: 'ADD_SPEAKER_ITEM',
      payload: {
        ...speaker
      }
  };
};

export const updateSpeakerItem = (speaker) => {
  return {
      type: 'UPDATE_SPEAKER_ITEM',
      payload: {
        ...speaker
      }
  };
};

export const deleteSpeakerItem = (speakerId) => {
  return {
      type: 'DELETE_SPEAKER_ITEM',
      payload: {
        speakerId
      }
  };
};

export const initSpeakerList = (speakerList) => {
  return {
      type: 'INIT_SPEAKER_LIST',
      payload: speakerList
  };
};

export const changePendingSavePs4 = (pendingSaved) => {
  return {
      type: 'CHANGED_PENDINNG_SAVE',
      payload: pendingSaved
  };
};

export const changeCurrentPlayedBlock = (currentPlayedBlock) => {
  return {
      type: 'CHANGED_CURRENT_PLAYED_BLOCK',
      payload: currentPlayedBlock
  };
};
