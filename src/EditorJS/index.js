import React, {Component} from 'react'
import { connect } from 'react-redux'
import _ from 'lodash';
import * as projectService from '../_services/project.service';
import 'react-html5video/dist/styles.css';
import PS4 from './PS4';
import {initSpeakerList, updateSpeakerItem, updatedSpeakerList} from '../_actions/speaker.actions';
import './style.css';
import localStorage from 'local-storage';

class EditorJS extends Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            transcriptionList : '',
            loadMoreButton: true,
            loadBackButton: false,
            currentArray:[],
            rawContent:[],
            /* PAGINATION WITH SORTING AND PAGING */
            page : 1, // input page, min value 1
            limit : 500, // input limit min value 1,
            videoImg:'',
            videoFilePath:'',
            updatedSpeakerList : [],
            isProjectLoading: true,
            isTranscriptionLoading: true,
        };
    }
    
    
    componentWillMount() {
        
        this.setState({updatedSpeakerList : localStorage.get('ss.ai.spl')});

        projectService.getProject()
            .then(response => {
                this.setState({videoImg:response.id});
                this.setState({videoFilePath:response.medias[0].videoFilePath});
                
            }) 
            .catch(error => {
                console.log("error >>" + error)
                this.state.isProjectLoading = false;
            })
        
        projectService.getAllTranscription()
            .then(response => {
                this.setState({transcriptionList : response});
                let totalSize = response.length;
                var currentTranscriptPart;
                
                if(response.length < this.state.limit)
                    this.state.loadMoreButton = false;

                if(totalSize > this.state.limit)  {
                    var firstEntry = _.chunk(response, this.state.limit);
                    currentTranscriptPart = firstEntry[0];
                    this.state.currentArray = currentTranscriptPart;
                }else
                    currentTranscriptPart = response;

                this.setState({rawContent : currentTranscriptPart});
                this.state.isTranscriptionLoading = false;
            })
            .catch(error => {
                console.log("error >>" + error);
                this.state.isTranscriptionLoading = false;
            })
    }
    
    render(){

        let ps4Content = "";

        if(this.state.rawContent.length > 0) {

            ps4Content = (
                <PS4
                rawContent = {projectService.createRawContent(this.state.rawContent)}
                transcriptionList = {this.state.transcriptionList}
                speakerList = {this.state.updatedSpeakerList}              
                />
            )
            
        }
        
        return (
            <div>
              {ps4Content}
           </div>
        );
    }
}


export default connect()(EditorJS);
  