
export default function secondsToTimeFilter() {

    function padTime(t) {
      return t < 10 ? '0' + t : t;
    }
  
    return function (_seconds) {
      if(isNaN(Number(_seconds)) || _seconds < 0)
        return '00:00';
  
      var hours = Math.floor(_seconds / 3600),
        minutes = Math.floor((_seconds % 3600) / 60),
        seconds = Math.floor(_seconds % 60);
  
      if (hours > 0)
        return hours + ':' + padTime(minutes) + ':' + padTime(seconds);
      else
        return padTime(minutes) + ':' + padTime(seconds);
    };
  }
  
  