
export const findEntitiesFunction = (type, editorState) => {
    return function (contentBlock, callback, contentState) {
      // console.log(contentBlock.getText().length);
      // console.log(contentBlock.getKey(), contentBlock.getText());
  
      const querySelector = `[data-block="true"][data-offset-key="${contentBlock.getKey()}-0-0"]`;
      const blockDomElement = document.querySelector(querySelector);
      const paddingTop = 150;
  
      // console.log('blockDomElement', blockDomElement);
      // console.log('isInViewport', isInViewport(blockDomElement, paddingTop));
  
      if (!blockDomElement || !isInViewport(blockDomElement, paddingTop)) {
        return;
      }
  
  
      contentBlock.findEntityRanges(
        (character) => {
          const entityKey = character.getEntity();
          return (
            entityKey !== null &&
            contentState.getEntity(entityKey).getType() === type
          );
        },
        callback
      );
    };
  };
  
  export function getEntitiesByBlockKey(editorState, blockKey, entityType = null) {
    const entities = [];
    const contentState = editorState.getCurrentContent();
    const block = contentState.getBlockForKey(blockKey);
  
    if (block) {
      let selectedEntity = null;
      block.findEntityRanges(
        (character) => {
          if (character.getEntity() !== null) {
            const entity = contentState.getEntity(character.getEntity());
            if (!entityType || (entityType && entity.getType() === entityType)) {
              selectedEntity = {
                entityKey: character.getEntity(),
                entity: contentState.getEntity(character.getEntity()),
              };
              return true;
            }
          }
          return false;
        },
        (start, end) => {
          entities.push({
            entityKey: selectedEntity.entityKey,
            ...selectedEntity.entity.getData(),
            text:  block.getText().substring(start, end),
            start,
            end
          });
        });
    }
  
    return entities;
  }
  
  function isInViewport(elem, paddingTop) {
    if (elem) {
      var bounding = elem.getBoundingClientRect();
      // console.log(bounding);
      return (
        bounding.bottom >= paddingTop &&
        // bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) + paddingTop
        // bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
      );
    }
  };
  