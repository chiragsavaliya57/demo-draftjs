import React, { Component } from 'react'
import _ from 'lodash';
import lodash from 'lodash';
import { connect } from 'react-redux'
import * as projectService from '../_services/project.service';
import TranscriptRow from './TranscriptRow';
import { RichUtils, CompositeDecorator, convertFromRaw, Editor, EditorState, Modifier } from 'draft-js';
import { createRawContent } from '../_services/project.service';
import { changePendingSavePs4, activateAutoScroll, selectPlayTime, initSpeakerList } from '../_actions/speaker.actions';
import 'react-html5video/dist/styles.css';
import { Link } from 'react-router-dom';
import { findEntitiesFunction } from './entity-helper';
import TimeCodedWord from './TimeCodeWord';
import ReactPlayer from 'react-player'
import $ from 'jquery';
import AutoScrollButton from '../AutoScrollButton/AutoScrollButton';
import { DefaultPlayer as Video } from 'react-html5video';
import 'react-html5video/dist/styles.css';

class PS4 extends Component {

  constructor(props) {
    super(props);

    let originalContentPlainText;
    const editorState = this.getInitialEditorState();
    if (editorState) {
      originalContentPlainText = editorState.getCurrentContent().getPlainText();
    }

    this.state = {
      editorState,
      hasAnyPopoverIsOpen: false,
      transcriptionList: this.props.transcriptionList,
      speakerList: this.props.speakerList,
      originalContentPlainText,
      start: 0,
      end: 999,
      startForBack: 0,
      endForBack: 999,
      loadMoreButton: true,
      loadBackButton: false,
      currentArray: [],
      /* PAGINATION WITH SORTING AND PAGING */
      page: 1, // input page, min value 1
      limit: 500, // input limit min value 1,
      videoImg: '',
      videoFilePath: '',
      source: '',
      currentPlayingTime: '',
      currentPlayingTimeForSec: '',
    };

    this.isScrolling = false;
    this.autoSaveTimer = null;
    this.popovers = {};

    this.fetchDataAndSet = this.fetchDataAndSet.bind(this);
    this.onChange = (editorState) => this.setState({ editorState });

    this.setEditor = (editor) => {
      this.editor = editor;
    };

    this.focusEditor = () => {
      if (this.editor) {
        this.editor.focus();
      }
    };

    this.onProgress = () => this._onProgress();

    this.turnOnAutoScroll = () => this._turnOnAutoScroll();
    this.scrollInto = (element, topOffset, callback, timeout) => this._scrollInto(element, topOffset, callback, timeout);

    this.handleReturn = (event) => this._handleReturn(event);
    this.handleScroll = (keepAutoScroll) => this._handleScroll(keepAutoScroll);
    this.handleScrollWithoutAutoScroll = () => this._handleScroll(false);
    this.saveTranscript = this.saveTranscript.bind(this);
    this.autoSaveChanges = (mediaId) => this._autoSaveChanges(mediaId);
    this.bookmarkChange = () => this._bookmarkChange();
    this.isCurrentContentDistinctThanOriginal = () => this._isCurrentContentDistinctThanOriginal();
    this.onPopoverInteraction = (popoverOpen, blockKey) => this._onPopoverInteraction(popoverOpen, blockKey);

    this.goToNextTranscription = (onlyWithBookmark) => this._goToNextTranscription(onlyWithBookmark);
    this.goToPreviousTranscription = (onlyWithBookmark) => this._goToPreviousTranscription(onlyWithBookmark);

    this.blockRendererFn = (contentBlock) => this._blockRendererFn(contentBlock);

    this.changeContent = this.changeContent.bind(this);
    this.loadPreviousContent = this.loadPreviousContent.bind(this);
    this.timeChangeForSec = this.timeChangeForSec.bind(this);
  }
  
  componentDidMount() {

    if (this.state.transcriptionList.length < this.state.limit)
      this.state.loadMoreButton = false;

    this.handleScroll(true);
    window.addEventListener('scroll', this.handleScrollWithoutAutoScroll);
    this.focusEditor();

  }

  componentWillMount() {

    projectService.getProject()
      .then(response => {
        this.setState({ videoImg: response.id, videoFilePath: response.medias[0].videoFilePath, source: response.medias[0].videoFilePath });

      })
      .catch(error => {
        console.log("error >>" + error)
      })
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollWithoutAutoScroll);
    window.onbeforeunload = null;
  }

  handleStateChange(state) {
    // copy player state to this component's state
    this.setState({
      player: state
    });
  }

  _scrollInto(element, topOffset = 0, callback, timeout) {
    const scrollTop = $(element).offset().top + topOffset;
    $('html, body').animate({
      scrollTop
    }, timeout, () => {
      if (lodash.isFunction(callback)) {
        callback();;
      }
    });
  }

  fetchDataAndSet(response) {
    let totalSize = response.length;
    var currentTranscriptPart;

    if (response.length < this.state.limit)
      this.state.loadMoreButton = false;

    if (totalSize > this.state.limit) {
      var firstEntry = _.chunk(response, this.state.limit);
      currentTranscriptPart = firstEntry[0];
      this.state.currentArray = currentTranscriptPart;
    } else
      currentTranscriptPart = response;

    let data = createRawContent(currentTranscriptPart);
    const initialContentState = convertFromRaw(data);
    this.setState({ editorState: EditorState.createWithContent(initialContentState) });

    console.log(response);
  }

  _turnOnAutoScroll() {
    this.props.activateAutoScroll(true);
    this.jumpToPlayedTime();
  }

  _handleScroll(keepAutoScroll) {
    this.isScrolling = true;
    this.props.activateAutoScroll(keepAutoScroll);

    window.clearTimeout(this.scrollingTimer);

    // Set a timeout to run after scrolling ends
    this.scrollingTimer = setTimeout(() => {
      this.isScrolling = false;
      // Run the callback
      // console.log('ff')
      const compositeDecorator = new CompositeDecorator([
        {
          strategy: findEntitiesFunction('TIME_CODED_WORD'),
          component: TimeCodedWord,
          props: {
            ps4State: this.state,
          }
        }
      ]);

      const editorState = this.state.editorState;
      const newEditorState = EditorState.set(editorState, { decorator: compositeDecorator });

      this.setState({ editorState: newEditorState });


    }, 250);
  }

  _isCurrentContentDistinctThanOriginal() {
    // console.log('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');

    // console.log('this.props.changedPendingSavePs4', this.props.changedPendingSavePs4);

    // console.log('this.state.originalContentPlainText');
    // console.log(this.state.originalContentPlainText);

    // console.log('this.state.editorState.getCurrentContent().getPlainText()');
    // console.log(this.state.editorState.getCurrentContent().getPlainText());

    // console.log('----------------------------------------------------------------------------------');

    return this.state.originalContentPlainText !== this.state.editorState.getCurrentContent().getPlainText();
  }

  _goToNextTranscription(onlyWithBookmark) {
    const blocks = this.state.editorState.getCurrentContent().getBlocksAsArray();
    let nextBlock;

    if (this.props.changedCurrentPlayedBlock) {
      const currentBlockKey = this.props.changedCurrentPlayedBlock.props.block.getKey();
      nextBlock = this.state.editorState.getCurrentContent().getBlockAfter(currentBlockKey);
    } else {
      nextBlock = blocks[0];
    }

    if (onlyWithBookmark && nextBlock) {
      const nextIndexBlock = blocks.findIndex(block => block.getKey() === nextBlock.getKey());
      const nextBlocks = blocks.slice(nextIndexBlock);
      nextBlock = nextBlocks.find(block => block.getData().get('bookmark'));
    }

    if (nextBlock) {
      const startTime = nextBlock.getData().get('startTime');
      this.props.selectPlayTime(startTime);
      this.turnOnAutoScroll();
    }
  }

  _goToPreviousTranscription(onlyWithBookmark) {
    const blocks = this.state.editorState.getCurrentContent().getBlocksAsArray();
    let previousBlock;
    let currentBlockKey

    if (this.props.changedCurrentPlayedBlock) {
      currentBlockKey = this.props.changedCurrentPlayedBlock.props.block.getKey();
      previousBlock = this.state.editorState.getCurrentContent().getBlockBefore(currentBlockKey);
    }

    if (onlyWithBookmark && currentBlockKey) {
      const currentIndexBlock = blocks.findIndex(block => block.getKey() === currentBlockKey);
      const previousBlocks = blocks.slice(0, currentIndexBlock).reverse();
      previousBlock = previousBlocks.find(block => block.getData().get('bookmark'));
    }

    if (previousBlock) {
      const startTime = previousBlock.getData().get('startTime');
      this.props.selectPlayTime(startTime);

      this.turnOnAutoScroll();
    }
  }

  _handleReturn(e) {
    const { editorState } = this.state;

    if (e.shiftKey && e.keyCode == 13) {
      this.setState({ editorState: RichUtils.insertSoftNewline(editorState) });
      return 'handled';
    } else {
      const selectionState = editorState.getSelection();
      const contentState = editorState.getCurrentContent();

      const start = selectionState.getStartOffset();
      const end = selectionState.getEndOffset();

      const blockKeyContainingEnd = selectionState.getEndKey();
      const contentBlockForEnd = contentState.getBlockForKey(blockKeyContainingEnd);

      if (end === contentBlockForEnd.getLength() || start === 0) {
        return 'handled';
      } else {
        this.handleScroll(false);
      }
    }

    return 'not-handled';
  }

  isInViewport(elem, paddingTop) {
    if (elem) {
      var bounding = elem.getBoundingClientRect();
      return (
        bounding.top >= paddingTop &&
        // bounding.left >= 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)
        // bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
      );
    }
  }

  jumpToPlayedTime() {
    let lastBlockKeyReached = this.getCurrentPlayedBlockKey();

    if (!lastBlockKeyReached) {
      lastBlockKeyReached = this.state.editorState.getCurrentContent().getFirstBlock().getKey();
    }

    const paddingTop = 150;
    const blockElement = document.querySelector(`[data-block="true"][data-offset-key="${lastBlockKeyReached}-0-0"]`);

    if (!this.isInViewport(blockElement, paddingTop)) {
      window.removeEventListener('scroll', this.handleScrollWithoutAutoScroll);

      this.scrollInto(blockElement, -150, () => {
        this.handleScroll(true);
        window.addEventListener('scroll', this.handleScrollWithoutAutoScroll);
      }, 500);
    }

  }

  _onProgress() {
    if (this.props.autoScrollActivated && !this.isScrolling) {
      this.jumpToPlayedTime();
    }
  }


  getCurrentPlayedBlockKey() {
    const blocks = this.state.editorState.getCurrentContent().getBlocksAsArray();

    let blockIndex = 0;
    let stopLoop = false;
    let lastBlockReached;

    while (!stopLoop && blockIndex < blocks.length) {
      blocks[blockIndex].findEntityRanges(
        (character) => {
          const entityKey = character.getEntity();
          if (entityKey) {
            const entity = this.state.editorState.getCurrentContent().getEntity(entityKey);
            const startTime = entity.getData().startTime
            if (startTime * 1000 <= this.state.currentPlayingTime) {
              lastBlockReached = blocks[blockIndex];
            } else {
              stopLoop = true;
            }
          }
        }
      );
      ++blockIndex;
    }

    if (lastBlockReached) {
      return lastBlockReached.getKey();
    }
  }

  _blockRendererFn(contentBlock) {
    const type = contentBlock.getType();

    if (type === 'TranscriptRow') {
      return {
        component: TranscriptRow,
        props: {
          ps4State: this.state,
          onChangeps4State: this.onChange,
          onPopoverInteraction: this.onPopoverInteraction,
          onUpdateMediaSpeaker: this.props.onUpdateMediaSpeaker,
          onAddMediaSpeaker: this.props.onAddMediaSpeaker,
          onBookmarkChange: this.bookmarkChange
        }
      };
    }
  }

  getAllIndexes(arr, val) {
    var indexes = [], i = -1;
    while ((i = arr.indexOf(val, i + 1)) != -1) {
      indexes.push(i);
    }
    return indexes;
  }

  trimLeft = function (str, charlist) {
    if (charlist === undefined)
      charlist = "\s";

    return str.replace(new RegExp("^[" + charlist + "]+"), "");
  }

  _bookmarkChange() {
    const transcriptRows = this._getTranscript();

    this.props.onBookmarkChange(transcriptRows)
  }

  _onPopoverInteraction(popoverOpen, blockKey) {

    this.popovers[blockKey] = popoverOpen;

    // console.log('//////////////////////////////', this.popovers);

    const hasAnyPopoverIsOpen = Object.values(this.popovers).find(isPopoverOpen => isPopoverOpen);

    this.setState({ hasAnyPopoverIsOpen });
  }

  handleKeyCommand = (command) => {
    const newState = RichUtils.handleKeyCommand(this.state.editorState, command);
    if (newState) {
      this.onChange(newState);
      return 'handled';
    }
    return 'not-handled';
  }


  getInitialEditorState() {
    let editorState;

    if (this.props.rawContent) {
      const initialContentState = convertFromRaw(this.props.rawContent);
      editorState = EditorState.createWithContent(initialContentState);
    } else {
      editorState = EditorState.createEmpty();
    }
    return editorState;
  }


  saveTranscript() {
    console.log("call")
    const contentState = this.state.editorState.getCurrentContent();

    const transcriptRows = {};
    var startTime, endTime, prevStartTime, prevEndTime;

    contentState.getBlocksAsArray().forEach(block => {
      let currentMetadata = null;
      let currentEntityKey = null;

      const blockkey = block.getKey();
      var blockText = block.getText();

      blockText = blockText.trimRight();
      const wordList = blockText.split(" ");
      const wordJsonList = [];
      var json;
      let index = 0;

      block.findEntityRanges(


        (character) => {
          const entityKey = character.getEntity();

          if (entityKey) {
            currentEntityKey = entityKey;
            const entity = this.state.editorState.getCurrentContent().getEntity(entityKey);
            currentMetadata = entity.getData();
            startTime = currentMetadata.startTime;
            endTime = currentMetadata.endTime;
            prevStartTime = startTime;
            prevEndTime = endTime;
          }
          else {
            startTime = prevStartTime;
            endTime = prevEndTime;
          }

          if (wordList[index] != undefined) {
            json = { "startTime": startTime, "endTime": endTime, "text": wordList[index] + " " };
            wordJsonList.push(json);
          }
          index++;
        }
      );

      if (wordList.length > wordJsonList.length) {

        for (let index = wordJsonList.length; index < wordList.length; index++) {
          if (wordList[index] != undefined) {
            json = { "startTime": prevStartTime, "endTime": prevStartTime, "text": wordList[index] + " " };
            wordJsonList.push(json);
          }
        }
      }

      transcriptRows[blockkey] = {
        wordsJson: wordJsonList,
        bookmark: block.getData().get('bookmark'),
        annotation: block.getData().get('annotation'),
        speaker: block.getData().get('speaker') ? JSON.parse(block.getData().get('speaker')) : null
      }

    });

    console.log("final tracription rows >> " + transcriptRows)

    var transcriptRowsNew = _.values(transcriptRows).map(function (transcript) {
      transcript.wordsJson = JSON.stringify(transcript.wordsJson)
      return transcript;
    });

    projectService.saveTranscription(transcriptRowsNew)
      .then((result) => {
        console.log(result);
        const originalContentPlainText = this.state.editorState.getCurrentContent().getPlainText();
        this.setState({ originalContentPlainText });
      })
      .catch(reason => {
        console.log(reason);
      });

  }

  changeContent() {

    this.setState({ editorState: EditorState.createEmpty() });

    /* PAGINATION WITH SORTING AND PAGING */
    const result = _(this.state.transcriptionList)
      .drop((this.state.page) * this.state.limit)   // page in drop function starts from 0
      .take(this.state.limit)                // limit 2
      .value();


    let data = createRawContent(result);
    const initialContentState = convertFromRaw(data);
    this.setState({ editorState: EditorState.createWithContent(initialContentState) });

    this.state.page = this.state.page + 1;

    if (result.length < this.state.limit)
      this.state.loadMoreButton = false;
    this.state.loadBackButton = true;

  }

  loadPreviousContent() {

    this.state.page = this.state.page - 1;
    this.setState({ editorState: EditorState.createEmpty() });

    /* PAGINATION WITH SORTING AND PAGING */
    const result = _(this.state.transcriptionList)
      .drop((this.state.page - 1) * this.state.limit)
      .take(this.state.limit)
      .value();

    let data = createRawContent(result);
    const initialContentState = convertFromRaw(data);
    this.setState({ editorState: EditorState.createWithContent(initialContentState) });

    if (this.state.page == 1)
      this.state.loadBackButton = false;
    this.state.loadMoreButton = true;
  }

  handleProgress = state => {
    //this.setState({ currentPlayingTime: state.playedSeconds })
    
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState(state)
    }
  }

  ref = player => {
    this.player = player
  }

  handleDuration = (duration) => {
    console.log('onDuration', duration)
    this.setState({ duration })
  }


  timeChangeForSec(time) {
    this.setState({ currentPlayingTime: time })
    this.state.currentPlayingTimeForSec = time;
    console.log(time + "new time update");
    this._onProgress();
  }
  render() {

    let loadMoreBackContent = "";

    if (this.state.loadBackButton) {
      loadMoreBackContent = (
        <button onClick={this.loadPreviousContent}>Previous</button>
      )
    }

    let loadMoreButtonContent = "";

    if (this.state.loadMoreButton) {
      loadMoreButtonContent = (
        <button onClick={this.changeContent}>Next</button>
      )
    }

    let videoContent = "";

    if (this.state.videoFilePath != "") {
      videoContent = (
        <div>
          <ReactPlayer
            width='50%' height='100%'
            ref={this.ref}
            url={this.state.videoFilePath}
            controls={true}
            onProgress={this.handleProgress}
            onDuration={this.handleDuration}
            
          />

          <p>OLD Player :: Current Playing Time For Above :- {this.state.currentPlayingTime}</p>

          <div style={{width:'50%'}}>

            <Video loop muted
                id="myVideo"
                ref="myVideo"
                onTimeUpdate={(event)=> {
                  this.timeChangeForSec(event.currentTarget.currentTime);
                }}
                controls={['PlayPause', 'Seek', 'Time', 'Volume', 'Fullscreen']}
                poster="http://sourceposter.jpg"
                onCanPlayThrough={() => {
                    // Do stuff
                }}>
                <source src={this.state.videoFilePath} type="video/mp4" />
            </Video>
            
          </div>
          
          <p>New Player :: Current Playing Time For Below :- {this.state.currentPlayingTimeForSec}</p>

        </div>
      );
    }

    let saveNow = "";
    if(this.isCurrentContentDistinctThanOriginal()) {
      saveNow = (
        <div className="save-now">
          <button onClick={this.saveTranscript}>Save Now</button>
        </div>
      )
    }

    return (
      <div>
        <Link to={"/"} >Go To Projects</Link>

        <div className="video-player-div">
          {videoContent}
        </div>

        {saveNow}

        <div className="button-container">
          <AutoScrollButton turnOnAutoScroll={this.turnOnAutoScroll} />
        </div>

        <div className="transcription-div">

          {loadMoreBackContent}

          <Editor
            readOnly={this.state.hasAnyPopoverIsOpen}
            handleReturn={this.handleReturn}
            blockRendererFn={this.blockRendererFn}
            editorState={this.state.editorState}
            onChange={this.onChange}
          />

          {loadMoreButtonContent}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    changedPendingSavePs4: state.changedPendingSavePs4,
    selectedPlayTime: state.selectedPlayTime,
    updatedSpeakerList: state.updatedSpeakerList,
    autoScrollActivated: state.autoScrollActivated.isActive,
    changedCurrentPlayedBlock: state.changedCurrentPlayedBlock
  };
};

// export default PS4;

export default connect(mapStateToProps, { changePendingSavePs4, activateAutoScroll, selectPlayTime, initSpeakerList }, null, { forwardRef: true })(PS4);
