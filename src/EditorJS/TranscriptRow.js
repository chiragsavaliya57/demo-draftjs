import React, {Component} from 'react'
import {connect} from 'react-redux'
import lodash from 'lodash';
import moment from 'moment';
import { EditorState, EditorBlock, Modifier, SelectionState, convertToRaw } from 'draft-js';

import { Popover, PopoverBody } from 'reactstrap';

import { changeCurrentPlayedBlock, changePendingSavePs4, selectPlayTime, updateSpeakerItem } from '../_actions/speaker.actions';
import secondsToTimeFilter from './seconds-to-time'

import initials from './initials.filter';

import SPEAKER_COLORS from './speaker-colors.constant';


class TranscriptRow extends React.Component {
    constructor(props) {
      super(props);
      let startEntityKey;
      let endEntityKey;
      let counterStartChar = 0;
      let counterEndChar = props.block.getLength();
  
      do {
        startEntityKey = props.block.getEntityAt(counterStartChar++);
      } while (!startEntityKey && counterStartChar < props.block.getLength());
  
      do {
        endEntityKey = props.block.getEntityAt(counterEndChar--);
      } while (!endEntityKey && counterEndChar > 0);
  
  
      this.state = {
        annotationPopoverOpen: false,
        speakerPopoverOpen: false,
        annotationValue: this.props.block.getData().get('annotation') || '',
        startTime: startEntityKey ? this.props.contentState.getEntity(startEntityKey).getData().startTime : null,
        endTime: endEntityKey ? this.props.contentState.getEntity(endEntityKey).getData().endTime : null,
        newSpeaker: { name: ''}
      }
  
      this.playTime = (event) => this._playTime(event);
  
      this.hasBookmark = () => this._hasBookmark();
  
      this.hasAnnotation = () => this._hasAnnotation();
  
      this.getSpeaker = () => this._getSpeaker();
      this.getNoRepeatedColor = () => this._getNoRepeatedColor();
  
      this.onChangeAnnotation = (event) => this._onChangeAnnotation(event);
      this.onAnnotationEnterPress = (event) => this._onAnnotationEnterPress(event);
  
      this.onChangeSpeaker = (index, value) => this._onChangeSpeaker(index, value);
      this.onChangeNewSpeaker = (event) => this._onChangeNewSpeaker(event);
  
      this.toggleBookmark = () => this._toggleBookmark();
      this.saveAnnotation = () => this._saveAnnotation();
  
      this.cancelAnnotationChange = () => this._cancelAnnotationChange();
  
      this.toggleAnnotationPopover = () => this._toggleAnnotationPopover();
      this.toggleSpeakerPopover = () => this._toggleSpeakerPopover();
  
      this.getNoSpeakerSymbol = () => this._getNoSpeakerSymbol();
      this.getSpeakerCssStyle = (speaker) => this._getSpeakerCssStyle(speaker);
  
      this.setTranscriptionSpeaker = (speaker) => this._setTranscriptionSpeaker(speaker);
      this.addSpeaker = () => this._addSpeaker();
      this.handleNewSpeakerKeyDown = (event) => this._handleNewSpeakerKeyDown(event);
  
    }
  
    componentDidMount() {

      this.delayedSpeakerUpdateCallback = lodash.debounce(speakerToUpdate => {
  
        this.props.blockProps.onUpdateMediaSpeaker(speakerToUpdate);
  
      }, 1500);
    }
  
    componentDidUpdate(prevProps, prevState) {
      if (prevState.annotationPopoverOpen !== this.state.annotationPopoverOpen) {
        this.props.blockProps.onPopoverInteraction(this.state.annotationPopoverOpen, this.props.block.key);
      }
      if (prevState.speakerPopoverOpen !== this.state.speakerPopoverOpen) {
        this.props.blockProps.onPopoverInteraction(this.state.speakerPopoverOpen, this.props.block.key);
      }
  
      if (this.props.highlightedText) {
        const currentTimeInSeconds = this.roundTime(this.props.highlightedText.currentTimeInSeconds);
        const startTime = this.roundTime(this.state.startTime);
        const endTime = this.roundTime(this.state.endTime);
  
        if (currentTimeInSeconds >= startTime && currentTimeInSeconds <= endTime) {
          this.props.changeCurrentPlayedBlock(this);
        }
      }
  
    }
  
    roundTime(num) {
      return Math.round(num * 100) / 100;
    }
  
    _playTime(event) {
      event.stopPropagation();
      this.props.selectPlayTime(this.state.startTime);
    }
  
    _toggleBookmark() {
      const bookmarkValue = !this.hasBookmark();
      const updatedEditorState = this.updateBlockData({
        ['bookmark']: bookmarkValue
      });
  
      this.props.blockProps.onChangeps4State(updatedEditorState, () => {
        this.props.changePendingSavePs4(true);
        this.props.blockProps.onBookmarkChange();
      });
    }
  
    _hasBookmark() {
      return !!this.props.block.getData().get('bookmark');
    }
  
    _hasAnnotation() {
      return !!this.props.block.getData().get('annotation');
    }
  
    _getSpeaker() {
      if (this.props.block.getData().get('speaker')) {
        const speaker = JSON.parse(this.props.block.getData().get('speaker'));
        return this.props.blockProps.ps4State.speakerList.find(speakerItem => speakerItem.id === speaker.id);
      }
    }
  
    _getNoRepeatedColor() {
      const availableColors = lodash.differenceBy(SPEAKER_COLORS, this.props.blockProps.ps4State.speakerList, 'color');
      if (availableColors[0]) {
        return availableColors[0].color;
      }
    }
  
    _onChangeAnnotation(event) {
      this.setState({ annotationValue: event.target.value });
    }
  
    _onAnnotationEnterPress(event) {
      if(event.keyCode == 13 && event.shiftKey == false) {
        event.preventDefault();
        this.saveAnnotation();
      }
    }
  
    _saveAnnotation() {
      const updatedEditorState = this.updateBlockData({
        ['annotation']: this.state.annotationValue
      });
  
      this.props.blockProps.onChangeps4State(updatedEditorState, () => {
        this.setState({ annotationPopoverOpen: false });
        this.props.blockProps.onSaveTranscript();
      });
    }
  
    _cancelAnnotationChange() {
      this.setState({ annotationValue: this.props.block.getData().get('annotation') || '', annotationPopoverOpen: false });
    }
  
    _onChangeSpeaker(index, value) {
      const speakerToUpdate = {
        ...this.props.blockProps.ps4State.speakerList[index],
        name: value
      };
  
      this.props.updateSpeakerItem(speakerToUpdate);
  
      this.delayedSpeakerUpdateCallback(speakerToUpdate);
    }
  
    _onChangeNewSpeaker(event) {
      this.setState({
        newSpeaker: {
          name: event.target.value
        }
      });
    }
  
    updateBlockData(data) {
      return EditorState.push(
        this.props.blockProps.ps4State.editorState,
        Modifier.mergeBlockData(
          this.props.contentState,
          SelectionState.createEmpty(this.props.block.key),
          data
        )
      );
    }
  
    getFormattedTimestamp() {
      const startTimecodeOffset = 11.12 || 0;
      const startTimecodeInSeconds = moment.duration(startTimecodeOffset)._milliseconds / 1000;
      return secondsToTimeFilter()(startTimecodeInSeconds + parseFloat(this.state.startTime));
    }
  
    _toggleAnnotationPopover() {
      this.setState({
        annotationPopoverOpen: !this.state.annotationPopoverOpen
      });
    }
  
    _toggleSpeakerPopover() {
      this.setState({
        speakerPopoverOpen: !this.state.speakerPopoverOpen
      });
    }
  
    _getNoSpeakerSymbol() {
      return '>';
    }
  
    _getSpeakerCssStyle(speaker) {
      const colorObject = SPEAKER_COLORS.find(function (speakerColorItem) {
        if (speaker) {
          return speakerColorItem.color === speaker.color;
        }
        return false;
      });
      if (colorObject) {
        return {
          backgroundColor: colorObject.color,
          color: colorObject.label
        };
      }
    }
  
    _setTranscriptionSpeaker(speaker) {
      const updatedEditorState = this.updateBlockData({
        ['speaker']: speaker ? JSON.stringify(speaker) : null
      });
  
      this.props.blockProps.onChangeps4State(updatedEditorState, () => {
        this.setState({speakerPopoverOpen: false});
        this.props.blockProps.onSaveTranscript();
      });
    }
  
    _addSpeaker() {
      if (!this.state.newSpeaker.name) {
        return;
      }
      const speakerToAdd = {...this.state.newSpeaker, name: this.state.newSpeaker.name.trim()};
  
      if (!speakerToAdd.name) {
        return;
      }
  
      speakerToAdd.color = this.getNoRepeatedColor();
  
      this.props.blockProps.onAddMediaSpeaker(speakerToAdd)
        .then(createdSpeaker => {
          this.setTranscriptionSpeaker(createdSpeaker);
        });
  
      this.setState({newSpeaker: { name: ''}});
    }
  
    _handleNewSpeakerKeyDown(e) {
      if (e.key === 'Enter') {
        this.addSpeaker();
      }
    }
  
    render() {
      // console.log('render row...');
      // console.log(this.getSpeaker());
      // console.log(this.props.block.getData());
      return (
        <div className={`transcription-row ${this.hasBookmark() ? 'bookmarked' : ''}`}>
  
          <div className="inner-toolbar inner-toolbar-left">
            <span
              className={`fa bookmark-icon fa-bookmark${!this.hasBookmark() ? '-o' : ''} ${this.hasBookmark() || this.state.annotationPopoverOpen ? ' visible' : ''}`}
              onClick={this.toggleBookmark}>
            </span>
          </div>

          <div className="inner-toolbar inner-toolbar-right annotation-row">
              <span id={'Popover-' + this.props.block.key}
                className={`notes-icon fa fa-sticky-note${!this.hasAnnotation() ? '-o' : ''} ${this.hasAnnotation() || this.state.annotationPopoverOpen ? ' visible' : ''}`}>
              </span>

              <Popover placement="right"
                trigger="legacy"
                isOpen={this.state.annotationPopoverOpen} target={'Popover-' + this.props.block.key}
                toggle={this.toggleAnnotationPopover}>
                <PopoverBody>
                  <div className="form-group annotation">
                    <label>Note</label>

                    <textarea
                      onKeyDown={this.onAnnotationEnterPress}
                      onChange={this.onChangeAnnotation}
                      value={this.state.annotationValue}
                      className="form-control">
                    </textarea>

                    <div className="row">
                      <div className="col-xs-12">
                        <a className="pull-right label-input-btn label-input-btn-cancel" onClick={this.cancelAnnotationChange}>
                          <span className="fa fa-times-circle"></span>
                        </a>
                        <a className="pull-right label-input-btn label-input-btn-ok" onClick={this.saveAnnotation}>
                          <span className="fa fa-check-circle"></span>
                        </a>
                      </div>
                    </div>

                  </div>
                </PopoverBody>
              </Popover>
          </div>
  
          <div className="transcription-data-container">
              <div className="inner-toolbar speaker-container" contentEditable={false} readOnly>
                <div
                  id={'Speaker-Popover-' + this.props.block.key}
                  className={`speaker ${!this.getSpeaker() ? 'default-speaker' : ''}`}
                  style={this.getSpeakerCssStyle(this.getSpeaker())}>
                  {this.getSpeaker() ? initials()(this.getSpeaker().name) : this.getNoSpeakerSymbol()}
                </div>
    
                <Popover placement="right"
                  trigger="legacy"
                  isOpen={this.state.speakerPopoverOpen} target={'Speaker-Popover-' + this.props.block.key}
                  toggle={this.toggleSpeakerPopover}>
                  <PopoverBody>
                    <input
                      onBlur={this.addSpeaker}
                      onKeyDown={this.handleNewSpeakerKeyDown}
                      maxLength="25"
                      placeholder="Add Speaker"
                      className="new-speaker-input"
                      onChange={this.onChangeNewSpeaker}
                      value={this.state.newSpeaker.name} />
    
                    <table className="speaker-table">
                      <tbody>
                        {this.getSpeaker() ? <tr>
                          <td className="speaker-initials-td" onClick={() => this.setTranscriptionSpeaker(null)}>
                            <div className="speaker-initials">
                              >
                            </div>
                          </td>
                          <td>
                            (No Speaker)
                          </td>
                        </tr> : null}
    
                        {
                          this.props.blockProps.ps4State.speakerList.map((speaker, index) => {
                            return (
                              <tr key={speaker.id}>
                                <td className="speaker-initials-td" onClick={() => this.setTranscriptionSpeaker(speaker)}>
                                  <div className="speaker-initials" style={this.getSpeakerCssStyle(speaker)}>
                                    {initials()(speaker.name)}
                                  </div>
                                </td>
                                <td>
                                  <input
                                    type="text"
                                    value={speaker.name}
                                    onChange={e => this.onChangeSpeaker(index, e.target.value)}
                                    maxLength="25" />
                                </td>
                              </tr>
                            );
                          })
                        }
    
                      </tbody>
    
                    </table>
                  </PopoverBody>
                </Popover>
              </div>
  
              <div className="transcription-time"
                onClick={this.playTime}
                contentEditable={false} readOnly>
                {this.getFormattedTimestamp()}
              </div>
    
              <div>
                <EditorBlock {...this.props} />
              </div>
            
          </div>
        </div>
  
  
      );
    }
  
  }
  
  const mapStateToProps = state => {
    return {
      changedMediaTimeCode: state.changedMediaTimeCode,
      updatedSpeakerList: state.updatedSpeakerList,
      highlightedText: state.highlightedText
    };
  };
  
  export default connect(mapStateToProps, { changeCurrentPlayedBlock, changePendingSavePs4, selectPlayTime, updateSpeakerItem })(TranscriptRow);
  