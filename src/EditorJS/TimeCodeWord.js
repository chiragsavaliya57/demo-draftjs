import React from 'react';
import { connect } from 'react-redux';
import { selectPlayTime } from '../_actions/speaker.actions';

class TimeCodedWord extends React.Component {
    constructor(props) {
        super(props);
        // console.log(props);
        // this.state = {
        //     startTime: this.props.contentState.getEntity(this.props.entityKey).getData().startTime,
        //     endTime: this.props.contentState.getEntity(this.props.entityKey).getData().endTime
        // }

        this.state = {};

        this.playTime = (event) => this._playTime(event)
        this.getStarTime = () => this._getStarTime();
        this.getEndTime = () => this._getEndTime();
    }

    _getStarTime() {
      return this.props.contentState.getEntity(this.props.entityKey).getData().startTime
    }

    _getEndTime() {
      return this.props.contentState.getEntity(this.props.entityKey).getData().endTime
    }

    getStyle() {
        let color = '#7a7a7a';
        if (this.props.ps4State.currentPlayingTime) {
            const currentTimeInSeconds = this.props.ps4State.currentPlayingTime;
            const startTimeInSeconds = this.getStarTime();

            if (currentTimeInSeconds && currentTimeInSeconds >= startTimeInSeconds) {
              color = '#0747A6';
            }
        }

        return {
          // background: (Math.floor(Math.random() * 2) + 1) % 2 ? 'red' : 'blue',
          color
      };
    }

    //REview if this is not preventing something
    // shouldComponentUpdate(nextProps, nextState) {
    //     if (nextProps.decoratedText !== this.props.decoratedText) {
    //         return true;
    //     }
    //     if (this.props.highlightedText && nextProps.highlightedText) {
    //         const currentTimeInSeconds = this.props.highlightedText.currentTimeInSeconds;
    //         const isCurrentlyHighlighted = currentTimeInSeconds && currentTimeInSeconds >= this.state.startTime;

    //         const nextCurrentTimeInSeconds = nextProps.highlightedText.currentTimeInSeconds;
    //         const willBeHighlighted = nextCurrentTimeInSeconds && nextCurrentTimeInSeconds >= nextState.startTime;

    //         return !!isCurrentlyHighlighted !== !!willBeHighlighted;
    //     }
    //     return false;

    // }

    _playTime(event) {
        if (event.ctrlKey || event.metaKey) {
            event.stopPropagation();
            // console.log(this.state.startTime);
            this.props.selectPlayTime(this.getStarTime());
        }
    }

    render() {
      // console.log('render');
        return (
            <span onClick={this.playTime}
                  // title={this.getStarTime() + ' - ' + this.getEndTime()}
                  style={this.getStyle()}
                  data-offset-key={this.props.offsetkey}>
                {this.props.children}
            </span>
        );
    }

}

const mapStateToProps = state => {
    return { highlightedText: state.highlightedText };
};

export default connect(mapStateToProps, { selectPlayTime })(TimeCodedWord);
